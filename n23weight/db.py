#
# N23 Weight - N23 framework weight tracking application
#
# Copyright (C) 2015-2022 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import asyncpg  # type: ignore[import-untyped]
from collections.abc import Sequence

SQL_USERS = 'select user_id, weight from "user" where user_id != 0'

async def read_users(db_uri: str) -> Sequence[tuple[int, float]]:
    """
    Read user configuration.

    :param db_uri: Database connection URI.
    """
    conn = await asyncpg.connect(db_uri)
    try:
        return (await conn.fetch(SQL_USERS))  # type: ignore[no-any-return]
    finally:
        await conn.close()

# vim: sw=4:et:ai
