#
# N23 Weight - N23 framework weight tracking application
#
# Copyright (C) 2015-2022 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Unit tests for processing weight measurement per user.
"""

from btzen import MiScaleWeightData, WeightFlags
from .. import core

import pytest

def weight(
        weight: float,
        stabilized: bool=True,
        load_removed: bool=False
) -> MiScaleWeightData:
    """
    Create task result with a weight measurement.
    """
    return MiScaleWeightData(WeightFlags.BMI, weight, stabilized, load_removed)

def test_create_user() -> None:
    """
    Test creating a user for given id and weight.
    """
    user = core.create_user(1, 50.0)
    assert 1 == user.id
    assert 45.0 == pytest.approx(user.min)
    assert 55.0 == pytest.approx(user.max)

def test_find_user() -> None:
    """
    Test finding a user id for a weight value.
    """
    users = [core.User(1, 50, 80), core.User(2, 30, 40)]
    uid = core.find_user(users, 31)
    assert 2 == uid

def test_find_user_special() -> None:
    """
    Test finding a user for a non-configured weight value.
    """
    users = [core.User(1, 50, 80), core.User(2, 30, 40)]
    uid = core.find_user(users, 45)
    assert 0 == uid

def test_weight_filter() -> None:
    """
    Test filtering weight measurements.
    """
    assert core.weight_filter(weight(30, True, False))
    assert not core.weight_filter(weight(30, True, True))
    assert not core.weight_filter(weight(30, False, True))
    assert not core.weight_filter(weight(30, False, False))

def test_assign_user() -> None:
    """
    Test assigning a user to a weight value.
    """
    users = [core.User(1, 50, 80), core.User(2, 30, 40)]

    result = core.assign_user(weight(55.0), users)
    assert 55.0 == pytest.approx(result.weight)
    assert 1 == pytest.approx(result.id)

    result = core.assign_user(weight(31.0), users)
    assert 31.0 == pytest.approx(result.weight)
    assert 2 == pytest.approx(result.id)

    # if no configured user found
    result = core.assign_user(weight(20.0), users)
    assert 20.0 == pytest.approx(result.weight)
    assert 0 == pytest.approx(result.id)

# vim: sw=4:et:ai
