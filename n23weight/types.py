#
# N23 Weight - N23 framework weight tracking application
#
# Copyright (C) 2015-2022 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
N23 Weight application types.
"""

import typing as tp
import dataclasses as dtc
from collections.abc import Sequence

class UserWeight(tp.NamedTuple):
    """
    Weight of a person with user id assigned.
    """
    id: int
    weight: float

@dtc.dataclass(frozen=True)
class User:
    """
    Weight scale user.

    :var id: User id.
    :var min: Minimum user weight.
    :var max: Maximum user weight.
    """
    id: int
    min: float
    max: float

Users: tp.TypeAlias = Sequence[User]

# vim: sw=4:et:ai
