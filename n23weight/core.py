#
# N23 Weight - N23 framework weight tracking application
#
# Copyright (C) 2015-2022 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
The N23 Weight demo application functions to process weight measurement per
user.
"""

import btzen
import operator
from functools import partial

from . import db
from .types import Users, User, UserWeight

range_min = partial(operator.imul, 0.9)
range_max = partial(operator.iadd, 5.0)

def create_user(id: int, weight: float) -> User:
    """
    Create user for the specified weight.
    """
    # id == 0 is special user id, so do not use it
    assert id != 0
    return User(id, range_min(weight), range_max(weight))

def find_user(users: Users, weight: float) -> int:
    """
    Find user for a weight value.

    Return `0` if user cannot be found.
    """
    items = (u.id for u in users if u.min <= weight <= u.max)
    return next(items, 0)

async def read_users(db_uri: str) -> Users:
    """
    Read users information from database.

    :param db_uri: Database connection URI.
    """
    data = await db.read_users(db_uri)
    return [create_user(u, w) for u, w in data]

def weight_filter(weight: btzen.MiScaleWeightData) -> bool:
    """
    Predicate to accept only stabilized weight information for non load
    removed events.
    """
    return bool(weight.stabilized and not weight.load_removed)

def assign_user(weight: btzen.WeightData, users: Users) -> UserWeight:
    """
    Assign user and weight information.

    If no configured user found, then user id is set to 0 (see also
    `find_user`).
    """
    uid = find_user(users, weight.weight)
    return UserWeight(uid, weight.weight)

# vim: sw=4:et:ai
