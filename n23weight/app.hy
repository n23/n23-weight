;
; N23 Weight - N23 framework weight tracking application
;
; Copyright (C) 2015-2024 by Artur Wroblewski <wrobell@riseup.net>
;
; This program is free software: you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation, either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;

(import btzen
        functools [partial]
        n23weight [read-users assign-user weight-filter])

(setv device (n23-config-get "n23-weight" "device"))
(setv db-uri (n23-config-get "n23-weight" "db-uri"))

(setv scale (btzen.weight device :make btzen.Make.MI_SMART_SCALE))

(setv users (await (read-users db-uri)))

(setv db (n23-sink-from-uri db-uri))
(db.add_entity "weight" ["user_id" "weight"])

(n23-add "weight"
         (partial (btzen.read.dispatch (type scale)) scale)
         (fmap (n23-> (?-> weight-filter)
                      (assign-user users)))
         db.write)

(n23-process (btzen.connect [scale]) db)

; vim: sw=2:et:ai
